class Dal
{

    constructor()
    {

    }

    connect(username, password, host = "bolt://localhost")
    {
        const neo4j = require('neo4j-driver').v1;
        this.driver = neo4j.driver(host, neo4j.auth.basic(username, password), {disableLosslessIntegers: true});
    }

    close()
    {
        this.driver.close();
    }

    // mapRecords(records)
    // {
    //     var mapped = {};
    //     mapped[]
    //
    // }

    mapRecord(record)
    {

        const obj = record.get(0).properties;
        obj.id = record.get(0).identity;
        obj.dtype = record.get(0).labels[0];
        const relationship = record.get(1);

        //obj[relationship.type] = this.createFunction(relationship.type);

        //return obj;
        return new Proxy(obj, handler);
    }


    match(request)
    {
        const session = this.driver.session();
        const strFilter = filter ? this.filterToString(filter) : "";
        const resultPromise = session.readTransaction(tx => tx.run(`MATCH (e:${entityName} ${strFilter})-[r]->(t) RETURN DISTINCT e,r`));

        return resultPromise.then(result => {

            session.close();
            return result.records.map(this.mapRecord);

        });
    }

    buildRequest(request)
    {
        for (let k in request)
        {

        }
    }

    create(entityName, entity)
    {
        const session = this.driver.session();
        const resultPromise = this.session.writeTransaction(tx => tx.run(`CREATE (e:${entityName}) RETURN e`));

        return resultPromise.then(result => {

            session.close();
            return result.records.map(this.mapRecord);

        });
    }




}

class Format
{
    static filterToString(filter)
    {
        var s = "{";

        for (let k in filter)
        {
            const val = filter[k];
            const strVal =  typeof val == "string" ? '"' + val + '"' : val;
            s += `${k}:${strVal}`;
        }
        s += "}";
        return s;
    }
}

class EntityRequest
{
    constructor(requestChunk, pObj)
    {
        this.requestChunk = requestChunk;
        this.pObj = pObj;
    }

    getPath()
    {
        var paths = [];

        if (this.pObj.getPath)
            paths = paths.concat(this.pObj.getPath());

        paths.push(this.requestChunk);

        return paths;
    }

    match()
    {
        const path = this.getPath();

        var s = "MATCH ";
        var nodes = path.map(x => `(e${x.name}:${x.name} {})`);
        nodes.join('-')
        for (let x of path)
        {
            s += `(e1:x.name)`
        }
        console.log(x);
        var s = "";
    }


}

var handler = {

    get: function(obj, prop) {
        return prop in obj ?
            obj[prop] :
            (filter) => new Proxy(new EntityRequest({name:prop, filter:filter}, obj), handler);
    }
};

module.exports = new Proxy({}, handler);