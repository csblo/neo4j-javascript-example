const neo4j = require('neo4j-driver').v1;
const [user, password] = ["neo4j", "admin"];
const uri = "bolt://localhost";
const driver = neo4j.driver(uri, neo4j.auth.basic(user, password));
const session = driver.session();

const resultPromise = session.readTransaction(tx => tx.run('MATCH (p:Person) RETURN p'));

resultPromise.then(result => {

  session.close();

  const record = result.records[0];
  const node = record.get(0);
  console.log(node.properties.firstname);

  // on application exit:
  driver.close();
  
});